import * as TYPES from './types'
import { ModerationService } from './moderation/moderationService'
import { MusicService } from './music/musicService'
import { ModerationHandler } from './moderation/moderationHandler'
import { MusicHandler } from './music/musicHandler'
import { ContainerModule } from 'inversify'
import { BozanicServer } from './bozanicServer'

export const bindings = new ContainerModule((bind) => {
  bind<BozanicServer>(TYPES.BozanicServer).to(BozanicServer).inSingletonScope()
})

export const configBindings = new ContainerModule((bind) => {
})

export const serviceBindings = new ContainerModule((bind) => {
  bind<ModerationService>(TYPES.ModerationService).to(ModerationService)
  bind<MusicService>(TYPES.MusicService).to(MusicService)
})

export const handlerBindings = new ContainerModule((bind) => {
  bind<ModerationHandler>(TYPES.Handler).to(ModerationHandler)
  bind<MusicHandler>(TYPES.Handler).to(MusicHandler)
})
