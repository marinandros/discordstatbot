import { IModerationService } from './interfaces'
import { Message, GuildMember } from 'discord.js'
import { injectable } from 'inversify'

@injectable()
export class ModerationService implements IModerationService {
  async banUser (msg: Message, gm: GuildMember): Promise<void> {
    if (gm.bannable) {
      gm.ban()
      msg.reply(`${gm.user.username} has been banned from the server!`)
    } else {
      msg.reply(`${gm.user.username} is a BO$$ and is not bannable from the server!`)
    }
  }

  async kickUser (msg: Message, gm: GuildMember): Promise<void> {
    if (gm.kickable) {
      gm.kick()
      msg.reply(`${gm.user.username} has been kicked from the server!`)
    } else {
      msg.reply(`${gm.user.username} is a BO$$ and is not kickable from the server!`)
    }
  }
}
