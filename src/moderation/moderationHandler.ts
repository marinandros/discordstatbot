import * as TYPES from '../types'
import { Message } from 'discord.js'
import { injectable, inject } from 'inversify'
import { IModerationService } from './interfaces'
import { IHandler } from '../interfaces'

@injectable()
export class ModerationHandler implements IHandler {
  name = 'Moderation'

  constructor (
    @inject(TYPES.ModerationService)
    private readonly moderationService: IModerationService
  ) {}

  canHandle (msg: Message): boolean {
    return msg.content.startsWith('!')
  }

  async handle (msg: Message): Promise<void> {
    const command = msg.content.split(' ')[0]
    const user = msg.mentions.users.first()
    if (!user) {
      msg.reply('No user has been mentioned inside your message!')
      return
    }
    if (!msg.guild) {
      msg.reply('No guild found for the attached message!')
      return
    }
    const gm = msg.guild.member(user)
    if (!gm) {
      msg.reply('No guild member found for the attached message!')
      return
    }
    switch (command) {
      case '!kick':
        await this.moderationService.kickUser(msg, gm)
        break
      case '!ban':
        await this.moderationService.banUser(msg, gm)
        break
      default:
        msg.reply(`Unknown command "${command.substring(1)}"`)
        break
    }
  }
}
