import {
  Message, GuildMember
} from 'discord.js'

export interface IModerationService{
  kickUser(msg: Message, gm: GuildMember): Promise<void>
  banUser(msg: Message, gm: GuildMember): Promise<void>
}
