import { Container } from 'inversify'
import {
  handlerBindings,
  serviceBindings,
  bindings
} from './container'

export class Application extends Container {
  private initialized = false

  async init (): Promise<Application> {
    if (!this.initialized) {
      this.initialized = true

      this.load(
        bindings,
        handlerBindings,
        serviceBindings
      )
    }
    return this
  }
}
