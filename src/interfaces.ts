import { Message } from 'discord.js'

export interface IPluginInfo {
  name: string
  version: string
}

export interface IHandler {
  name: string
  canHandle (msg: Message): boolean
  handle (msg: Message): void
}
