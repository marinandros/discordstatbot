import 'reflect-metadata'
import * as TYPES from './types'
import { Application } from './application'
import { BozanicServer } from './bozanicServer'
require('dotenv').config()

;(async function main (): Promise<void> {
  const application = new Application()
  await application.init()
  const server = application.get<BozanicServer>(TYPES.BozanicServer)
  await server.start()
})().catch(err => {
  // eslint-disable-next-line no-console
  console.error('Error starting application: %s', err.message)
  process.exit(1)
})
