import {
  Channel, VoiceChannel,
  VoiceConnection, Message,
  TextChannel, Guild, StreamDispatcher
} from 'discord.js'

export interface IMusicService {
  play(
    msg: Message,
    guild: Guild,
    vc: VoiceChannel,
    urlOrName: string,
    channel: TextChannel
  ): Promise<void>
  start(
    msg: Message,
    guild: Guild,
    vc: VoiceChannel,
    urlOrName: string,
    channel: TextChannel,
    queue: Queue
  ): Promise<void>
  skip(
    msg: Message,
    guild: Guild,
    vc: VoiceChannel,
    urlName: string,
    channel: TextChannel
  ): Promise<void>
  pause(msg: Message): Promise<void>
}

export interface Song {
  title: string
  url: string
}

export interface StreamInformation {
  stream: StreamDispatcher
  playing: boolean
}

export interface Queue {
    textChannel: Channel
    voiceChannel: VoiceChannel
    connection: VoiceConnection | null
    stream: StreamDispatcher | null
    songs: Song[]
    volume: number
    playing: boolean
}
