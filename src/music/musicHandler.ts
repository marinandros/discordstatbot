import * as TYPES from '../types'
import { injectable, inject } from 'inversify'
import {
  Message, GuildMember, TextChannel, DMChannel, NewsChannel, VoiceChannel
} from 'discord.js'
import { IHandler } from '../interfaces'
import { IMusicService } from './interfaces'

@injectable()
export class MusicHandler implements IHandler {
  name = 'Music'

  constructor (
    @inject(TYPES.MusicService)
    private readonly musicService: IMusicService
  ) {}

  canHandle (msg: Message): boolean {
    if (msg.channel.type === 'text') {
      if ((msg.channel as TextChannel).name === 'testing' || (msg.channel as TextChannel).name === 'music-bot-channel') {
        return msg.content.startsWith('$')
      } else {
        return false
      }
    }
    return false
  }

  async handle (msg: Message): Promise<void> {
    const command = msg.content.split(' ')
    if (!msg.member) {
      msg.reply('No member found when parsing the message')
      return
    }
    if (!msg.member.voice.channel) {
      await msg.reply('you need to be in a voice channel to play music!')
      return
    }
    if (msg.channel.type !== 'text') {
      msg.reply('This command can only be sent from a textual channel')
      return
    }
    if (!msg.guild) {
      msg.reply('No guild found when parsing the message')
      return
    }
    if (!await this.checkPermission(msg, msg.member.voice.channel, msg.member, msg.channel)) return
    switch (command[0]) {
      case '$play':
        await this.musicService.play(
          msg,
          msg.guild,
          msg.member.voice.channel,
          command[1],
          msg.channel as TextChannel) // can explicitly cast as we know where the message is coming from
        break
      case '$pause':
        await this.musicService.pause(msg)
        break
      // case '$resume':
      //   await this.musicService.resume(msg)
      //   break
      case '$skip':
        await this.musicService.skip(
          msg,
          msg.guild,
          msg.member.voice.channel,
          command[1],
          msg.channel as TextChannel)
        break
      default:
        msg.reply(`unknown command "${command[0].substring(1)}"`)
        break
    }
  }

  async checkPermission (msg: Message, vc: VoiceChannel, member: GuildMember, channel: TextChannel | DMChannel | NewsChannel): Promise<boolean> {
    const permissions = vc.permissionsFor(member)
    if (!permissions) {
      await msg.reply('you need some permissions buddy!')
      return false
    }

    if (!permissions.has('CONNECT') || !permissions.has('SPEAK')) {
      await channel.send('I need the permissions to join and speak in your voice channel!')
      return false
    }
    return true
  }
}
