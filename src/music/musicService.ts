import { IMusicService, Queue, Song, StreamInformation } from './interfaces'
import { injectable } from 'inversify'
import {
  Message, VoiceChannel,
  TextChannel, Guild
} from 'discord.js'
import YoutubeDiscord from 'ytdl-core-discord'

@injectable()
export class MusicService implements IMusicService {
  private serverMusicQueue: Map<string, Queue>

  constructor () {
    this.serverMusicQueue = new Map<string, Queue>()
  }

  async play (
    msg: Message,
    guild: Guild,
    voiceChannel: VoiceChannel,
    urlOrName: string,
    textChannel: TextChannel): Promise<void> {
    if (!YoutubeDiscord.validateURL(urlOrName)) {
      msg.reply('provide a valid url you stinker!')
      return
    }
    const songInfo = await YoutubeDiscord.getInfo(urlOrName)

    const song: Song = {
      title: songInfo.title,
      url: songInfo.video_url
    }

    // Guild is present because of the checks done in the handler
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    let playList = this.serverMusicQueue.get(msg.guild!.id)
    if (!playList) {
      const queue: Queue = {
        textChannel: msg.channel,
        // Voice channel is present because of the checks done in the handler
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        voiceChannel: voiceChannel,
        connection: null,
        stream: null,
        songs: [],
        volume: 5,
        playing: false
      }

      // generate connection for this bot on the voice channel
      try {
        const conn = await voiceChannel.join()
        queue.connection = conn
      } catch (e) {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        textChannel.send(e.message)
        return
      }
      queue.songs.push(song)
      this.serverMusicQueue.set(guild.id, queue)
      playList = queue
    } else {
      playList.songs.push(song)
      msg.reply(`"${song.title}" has been successfully added to the queue`)
    }
    this.start(
      msg,
      guild,
      voiceChannel,
      urlOrName,
      textChannel,
      playList
    )
  }

  async start (
    msg: Message, guild: Guild,
    vc: VoiceChannel, urlName: string,
    tc: TextChannel, musicQueue: Queue
  ): Promise<void> {
    if (!musicQueue.playing) {
      musicQueue.playing = true
      const stream = await YoutubeDiscord(musicQueue.songs[0].url)
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const dispatcher = musicQueue.connection!.play(stream, { type: 'opus' })
      musicQueue.stream = dispatcher
      dispatcher.on('finish', () => {
        musicQueue.playing = false
        musicQueue.songs.shift()
        if (musicQueue.songs.length !== 0) {
          msg.reply(`playing ${musicQueue.songs[0].title}`)
          this.start(msg, guild, vc, urlName, tc, musicQueue)
        } else {
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          musicQueue.connection!.disconnect()
        }
      }).on('error', (error: any) => {
        msg.reply(`Got an error: ${error.message}`)
      }).setVolumeLogarithmic(musicQueue.volume / 5)
    }
  }

  async pause (msg: Message): Promise<void> {
    const streamInfo = this.checkGuild(msg)
    if (streamInfo) {
      streamInfo.playing = false
      streamInfo.stream.pause()
      msg.reply('paused music')
    }
  }

  async continue (msg: Message): Promise<void> {
    const streamInfo = this.checkGuild(msg)
    if (streamInfo) {
      streamInfo.playing = false
      streamInfo.stream.resume()
      msg.reply('resumed playing music')
    }
  }

  async skip (
    msg: Message, guild: Guild,
    vc: VoiceChannel, urlName: string,
    tc: TextChannel
  ): Promise<void> {
    const musicQueue = this.serverMusicQueue.get(guild.id)
    if (!musicQueue) {
      msg.reply('There is no queue, so there is nothing to skip you dumbass.')
      return
    }
    musicQueue.songs.shift()
    musicQueue.playing = false
    if (musicQueue.songs.length === 0) {
      msg.reply('this was the final song, farewell now')
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      musicQueue.connection!.disconnect()
      return
    }
    msg.reply('skipping the damn song')
    this.start(msg, guild, vc, urlName, tc, musicQueue)
  }

  checkGuild (msg: Message): StreamInformation | null {
    if (msg.guild) {
      const queue = this.serverMusicQueue.get(msg.guild.id)
      if (!queue) {
        msg.reply('no queue found my guy.')
        return null
      }
      const { stream, playing } = queue
      if (!playing) {
        msg.reply('cannot pause if it\'s not playing dummy')
        return null
      }
      if (!stream) {
        msg.reply('no stream found on the found queue boiiii')
        return null
      }
      return {
        playing: playing,
        stream: stream
      }
    } else {
      msg.reply('something went wrong while fetching the guild my guy.')
      return null
    }
  }
}
