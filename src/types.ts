export const Handler = Symbol.for('Handler')

export const BozanicServer = Symbol.for('BozanicServer')

export const ServerConfiguration = Symbol.for('ServerConfiguration')

export const ModerationService = Symbol.for('ModerationService')
export const ModerationController = Symbol.for('ModerationController')

export const MusicService = Symbol.for('MusicService')
export const MusicController = Symbol.for('MusicController')

