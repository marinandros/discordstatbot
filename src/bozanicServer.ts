import * as TYPES from './types'
import { IHandler } from './interfaces'
import { SEVERITY } from './utils/severityMap'
import { Client, Message } from 'discord.js'
import { injectable, multiInject } from 'inversify'

@injectable()
export class BozanicServer {
  private initialized = false
  private client!: Client

  constructor (
    @multiInject(TYPES.Handler)
    private readonly handlers: IHandler[]
  ) {}

  async init (): Promise<void> {
    if (this.initialized) return
    this.initialized = true
    this.client = new Client()
    this.client.on('ready', () => {
      // eslint-disable-next-line no-console
      if (this.client.user) {
        console.log(`Logged in as a client ${this.client.user.tag}`)
      } else {
        console.log('Simply logged in')
      }
    })
    this.client.on('message', msg => {
      if (this.client.user && msg.author.id !== this.client.user.id) {
        for (const handler of this.handlers) {
          if (handler.canHandle(msg)) {
            handler.handle(msg)
          }
        }
      }
    })
    this.client.login(process.env.DISCORD_TOKEN)
    // eslint-disable-next-line no-console
    console.log('Server start called')
  }

  async start (): Promise<void> {
    await this.init()
  }

  async doSomethingBasedOnSeverity (severity: SEVERITY, msg: Message): Promise<void> {
    switch (severity) {
      case SEVERITY.LOW: {
        msg.reply('Come on bud')
        break
      }
      case SEVERITY.MEDIUM: {
        msg.reply('Now we\'re talking')
        break
      }
      case SEVERITY.HIGH: {
        msg.reply('Take a chill pill')
        break
      }
      case SEVERITY.HOLYSHIT: {
        msg.reply('You dirty slag')
        break
      }
      default: {
        break
      }
    }
  }
}
